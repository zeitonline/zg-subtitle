//christopher pietsch
//christopher.pietsch@zeit.de

const request = require('request-promise');
const fs = require('fs');
const cheerio = require('cheerio');
var csv = require("fast-csv");

const interval = 4000
const crawlInterval = 200
// const startTime = 300000
// const startTime = 4276000
// const startTime = 11040000
// const startTime = 72000
const startTime = 336000
// const startTime = 1527365380000
let time
// const startTime = 1527364364000 - (1000 * 50)
// const startTime = 1527603816000
// const startTime = 1527360284000
// const test = fs.readFileSync('./test.xml', 'utf8')

read()

async function read(){
  let i = 0
  time = startTime
  let lastSubtitle = ''
  const file = 'data/' + startTime + '-' + (+new Date()) + '.csv'
  var csvStream = csv.createWriteStream({headers: true})
  var writableStream = fs.createWriteStream(file);
  csvStream.pipe(writableStream);

  console.log('starting at ', (new Date(startTime)).toDateString())
  
  // while(++i < 10){
  while(true){
    const subs = await getXml(time)
        .then(parseXml)
        .then(wait)

    time += interval

    subs.forEach((sub,i) => {
      if(sub.text === lastSubtitle) {
        console.log('duplicate')
      } else {
        csvStream.write(sub);
      }
      console.log(sub)
      lastSubtitle = sub.text
    })
  }
  
  console.log('wrote into ', file)
  csvStream.end()
}


function parseXml(data){
  const subtitles = []
  const $ = cheerio.load(data, {
    xmlMode: true
  })

  $('div p').each(function() {
    const d = $(this)
    subtitles.push({
      time,
      id: d.attr('xml:id'),
      begin: d.attr('begin'),
      end: d.attr('end'),
      text: d.text().trim()
    })
  })
  return subtitles
}

function wait(data){
  return new Promise((r,j) => {
    setTimeout(() => { r(data) }, crawlInterval)
  }) 
}

function getXml(timestamp){
  const url = getUrl(timestamp)
  console.log((new Date(timestamp)).toTimeString().substring(0, 8), timestamp)

  return request({
    url: url,
    method: 'GET',
    headers : {
      'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'
    }
  })
  .then(d => d.substring(d.indexOf('<?xml')))
  .then(d => d.replace(/<br\/>/g, ' '))
  .catch(function (err) {
      console.log(err.statusCode)
      return ''
  })
}

function addSubtitles(subs){
  const subtitles = []
  const unique = []
  for(let [i, sub] of subs){
    if(i === 0 && sub == subtitles[subtitles.length - 1].text){
      console.log('duplicate')
      return
    }
    subtitles.push(sub)
    unique.push(sub)
  }
  return unique
}

function getUrl(t){
  // return 'https://zattoo-dash-live.akamaized.net/ZDF/subs-time\=' + timestamp + '-1500000-0.m4s\?z32\=MNZWSZB5GE2TGMRUHFATGNZXIM3UCMJYIMWTONZRIQ2TQRBUHE2EGMSGIVBUCJTOMF2GS5TFHUYTKMBQEZ2XGZLSL5UWIPJXG4YDENRRGMTHG2LHHU2V6MRUGI2GMZDFGE2GIYLCHBQTONJXGI4GIOLFMU3TEZJYG4YDAZLD'
  // return 'https://zba1-0-dash-live.zahs.tv/ARD/subs-time=' + t + '-1500000-0.m4s?z32=MNZWSZB5GE2TGMZSGMYUGMZXGA3TAMRWGQWTERBTGQYTORCDGQYECNRTHBCDMJTOMF2GS5TFHUYTKMBQEZ2XGZLSL5UWIPJRGA2TQNJSGITHG2LHHU2V6YJTG5RTGNJXGEYWGOJTG5SGIYLCHE2TINDDMNSDAZJQGE3DKYLC'
  // return 'https://fr5-4-dash-pvr.zahs.tv/HD_zdf/1527358800/1527369600/subs-time=' + t + '-5000000-0.m4s?z32=MNZWSZB5GE2TGNBQIE3UEMRRGEZDGMCFGQWTON2FINBUGRCGII4EGRJQIEYTIJTOMF2GS5TFHU2TAMBQEZ2XGZLSL5UWIPJSGU3DGMRWGEZSM43JM46TGMC7MMZWIZJWGAYDENJQHA2GEY3GGE2WIMJQHAZWEOBRGJQWEYJWGY4A'
  // return 'https://fra3-1-dash-pvr.zahs.tv/HD_zdf/1531666500/1531677000/subs-time='+ t +'-5000000-0.m4s?z32=MNZWSZB5GE2TIMJZIJBDMNBXGMYDCQZWG4WTOOJZGJBTIQ2CHAYTAQKGGVASM3TBORUXMZJ5GUYDAMBGOVZWK4S7NFSD2MRVGYZTENRRGMTHG2LHHUZTAX3FGA3DIM3BGEYDCMZXGJSDKNTBGBSTCMBZMYYGMNRUGBRTENRRMI'
  return 'https://fr5-2-dash-pvr.zahs.tv/HD_zdf/1531663200/1531668000/subs-time='+ t + '-5000000-0.m4s?z32=MNZWSZB5GE2TIMJZIQYUGQJZIZCUGMRQGIWTKNCCGJCDENRSIYZUKMRXIM3DAJTOMF2GS5TFHU2TAMBQEZ2XGZLSL5UWIPJSGU3DGMRWGEZSM43JM46TGMC7MQ3TEMJTGIYTKYZZMUZWGOJZGE3DAYRTMJSTGYZRGM4DSZTEGMYQ'
}
