var fs = require('fs');
var csv = require("fast-csv");

var subs = []
var pause = 10
var lastTime = 0

var text = ""

function load(){
   csv
     // .fromPath('data/336000-1531681959734.csv', {headers: true})
     .fromPath('data/72000-1531678840771.csv', {headers: true})
     .on("data", function(data){
         // const time = data.begin.split('.')[0].split(':')[2]
         var diff = (data.time - lastTime) / 1000
         console.log(diff, lastTime, data.time, data.text)
         if(diff > pause) { text += "\n" + data.text }
         else { text += " " + data.text }
         // subs.push(data)
         lastTime = data.time
     })
     .on("end", () => {
       // console.log(subs)
       // console.log(text)
       // fs.writeFileSync("data/336000-1531681959734.txt", text, 'utf8')
       fs.writeFileSync("data/72000-1531678840771.txt", text, 'utf8')
     })
}

load()